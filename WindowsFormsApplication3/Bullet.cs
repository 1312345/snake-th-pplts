﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GamesSnake
{
    public class Bullet
    {
        #region thành phần dữ liệu
  
        public Point[] viTri = new Point[3];
        private Point[] tailLast = new Point[3];
        /// <summary>
        /// do dai cua con ran mat dinh bang 3
        /// </summary>
        public int doDai=2;
        /// <summary>
        /// màu sắc đầu con rắn
        /// </summary>
        Brush mauDan;
        /// <summary>
        /// màu sắc thân con rắn
        /// </summary>
        /// <summary>
        /// màu sắc đường viền ngoài con rắn
        /// </summary>
        Pen vienNgoai;
        /// <summary>
        /// hướng di chuyển của rắn, trái|phải|trên|dưới
        /// </summary>
        public DiChuyen diChuyen { get; set; }

        /// <summary>
        /// nơi cần vẽ lên|nơi con rắn ở :D
        /// </summary>
        Graphics g;
        #endregion

        /// <summary>
        /// khởi tạo các giá trị
        /// </summary>
        /// <param name="gr">vùng dùng để vẽ con ran len</param>
        /// <param name="vTri">tọa độ ban đầu cần vẽ(cái đầu của nó :D)</param>
        /// <param name="dr">màu sắc cái đầu rắn</param>
        /// <param name="th">màu sắc thân rắn</param>
        /// <param name="vn">màu sắc đường viền con rắn :D</param>
        public Bullet(Graphics gr, Point vTri, DiChuyen huong)
        {
            g = gr;
            mauDan = Brushes.Black;
            vienNgoai = Pens.White;
            diChuyen = huong;
            if (diChuyen == DiChuyen.trai)
            {
                viTri[1].Y = viTri[2].Y = vTri.Y;
                viTri[1].X = vTri.X - 2;
                viTri[2].X = vTri.X - 1;
            }
            else if (diChuyen == DiChuyen.phai)
            {
                viTri[1].Y = viTri[2].Y = vTri.Y;
                viTri[1].X = vTri.X + 2;
                viTri[2].X = vTri.X + 1;
            }
            if (diChuyen == DiChuyen.len)
            {
                viTri[1].X = viTri[2].X = vTri.X;
                viTri[1].Y = vTri.Y - 2;
                viTri[2].Y = vTri.Y - 1;
            }
            else if (diChuyen == DiChuyen.xuong)
            {
                viTri[1].X = viTri[2].X = vTri.X;
                viTri[1].Y = vTri.Y + 2;
                viTri[2].Y = vTri.Y + 1;
            }
            
            
            

        }
        #region các phương thức 

        public void ve()
        {
            for (int i = 1; i <= doDai; i++)
                //ve phan than con ran
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), vienNgoai, mauDan);
            
        }

        public void move()
        {
            tailLast = viTri; //new Point(viTri[doDai].X * 10, viTri[doDai].Y * 10);
            //xoa duoi
            for (int i = 1; i < 3; i++)
            {
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), Pens.White, Brushes.White);
            }
            
            
            //ve dau ran
            if (diChuyen == DiChuyen.trai)
            {
                viTri[1].X -= 2;
                viTri[2].X -= 2;
            }
            else if (diChuyen == DiChuyen.phai)
            {
                viTri[1].X += 2;
                viTri[2].X += 2;
            }
            if (diChuyen == DiChuyen.len)
            {
                viTri[1].Y -= 2;
                viTri[2].Y -= 2;
            }
            else if (diChuyen == DiChuyen.xuong)
            {
                viTri[1].Y += 2;
                viTri[2].Y += 2;
            }
            //ve tu duoi ve toi dau
            for (int i = doDai; i >= 1; i--)
            {
                
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), vienNgoai, mauDan);
            }

        }

        
        public int EatSnake(snake SnakeEatten)
        {
            Point head = viTri[1];
            Point body = viTri[2];
            Point[] Snake2 = SnakeEatten.viTri;
            for (int i = 1; i < Snake2.Length; i++)
                if (Snake2[i] == head || Snake2[i] == body)
                    return i;
            return -1;
        }

        public bool EatWall(MaTran mt)
        {
            Point head = viTri[1];
            Point body = viTri[2];

            if (mt.mt[head.X,head.Y] == 1 || mt.mt[body.X,body.Y] == 1)
                    return true;
            return false;
        }

        public bool validRange()
        {
            return (viTri[1].X < 50 && viTri[1].Y < 50 && viTri[1].X >= 0 && viTri[1].Y >= 0);
            
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            for (int i = 1; i <= doDai; i++)
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), Pens.White, Brushes.White);
        }

        #endregion
        
    }
}
