﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace GamesSnake
{
    public partial class Form1 : Form
    {
        VatCan vc;
        Graphics g;
        snake conRan, conRan2;
        MaTran phamVi;
        ThucAn thucAn;
        private ItemMap itemmap;
        delegate void ThongTin();

        ThongTin gameOver;
        
        ThongTin ketThuc;
        ThongTin demTime;
        private Bullet[] b = new Bullet[2];
        private wall[] w = new wall[2];
        private int timer = 0;
        private int[] slow = new int[] {0,0};
        private string mapType = "level1.lev";

        /// <summary>
        /// tốc độ di chuyển
        /// </summary>
        private Timer tocDo;
        public Form1()
        {
            InitializeComponent();
            panel1.Width = 503;
            panel1.Height = 503;
            g = panel1.CreateGraphics();
            gameOver = GameOver;
            
            ketThuc = KetThuc;
            demTime = DemTime;
            
            
            reset();
            cbbMap.SelectedIndex = 0;
            cbbTime.SelectedIndex = 0;
        }

        private void MainGame()
        {
            //kiem tra có hết time chưa
            if (timer == 1)
            {
                Go(false);             
                this.Invoke(gameOver);
                if (conRan.doDai > conRan2.doDai)
                    MessageBox.Show("Player 1 Win");
                else if
                    (conRan.doDai < conRan2.doDai)
                    MessageBox.Show("Player 2 Win");
                else
                    MessageBox.Show("Tie game");
            }
                
            //kiem tra con ran co can nhau ko
            int t = conRan.SnakeEatSnake(conRan2);
            int t2 = conRan2.SnakeEatSnake(conRan);
            if (t != -1) // Con 1 an con 2
            {
                diappearAll();
                conRan.Eat(conRan2.doDai - t);
                conRan2.beEatten(conRan2.doDai - t);
                //return;
            }
            else if (t2 != -1) //Con 2 an con 1
            {
                diappearAll();
                conRan2.Eat(conRan.doDai - t2);
                conRan.beEatten(conRan.doDai - t2);
                //return;
            }
            //kiem tra đan co dung ran khong
            if (b[1] != null)
            {
                b[1].move();
                if (!b[1].validRange())
                {
                    b[1].Dispose();
                    b[1] = null;
                }
                else
                {
                    t = b[1].EatSnake(conRan);
                    if (t != -1) // conRan1 bi dan ban trung
                    {
                        snakeDisappear(conRan);
                        conRan.beEatten(conRan.doDai - t);
                        b[1].Dispose();
                        b[1] = null;
                        conRan2.skill = ItemMap.skills.none;
                    }

                    if (b[1] != null)
                        if (b[1].EatWall(phamVi))
                        {
                        phamVi.Change(b[1].viTri, 0);
                    }
                }
                //xuly
            }
            if (b[0] != null)
            {
                b[0].move();
                if (!b[0].validRange())
                {
                    b[0].Dispose();
                    b[0] = null;
                }
                else
                {

                    t = b[0].EatSnake(conRan2);
                    if (t != -1) // conRan2 bi dan ban trung
                    {
                        snakeDisappear(conRan2);
                        conRan2.beEatten(conRan2.doDai - t);
                        b[0].Dispose();
                        b[0] = null;
                        conRan.skill = ItemMap.skills.none;
                    }

                    if (b[0] != null)
                    if (b[0].EatWall(phamVi))
                    {
                        phamVi.Change(b[0].viTri, 0);
                    }
                    //xuly
                }
            }

            //con ran 1 di chuyen
            if(slow[1] %2 == 0)
            {
                conRan.move();
                Point[] vtr = new Point[conRan.doDai];
                for (int i = 1; i <= conRan.doDai; i++)
                    vtr[i - 1] = conRan.viTri[i];
                conRan_GoiSuLi(vtr, conRan.tailLast);
            }

            //con ran 2 di chuyen
            if(slow[0]%2 ==0)
            {
                conRan2.move();
                Point[] vtr = new Point[conRan2.doDai];
                for (int i = 1; i <= conRan2.doDai; i++)
                    vtr[i - 1] = conRan2.viTri[i];
                conRan_GoiSuLi2(vtr, conRan2.tailLast);
            }
            if (slow[0] != 0)
            {
                slow[0]--;
            }
            if (slow[1] != 0)
            {
                slow[1]--;
            }
        }

        private void DemTime()
        {
            timer--;
            txtTime.Text = timer.ToString();
            Diemp1.Text = conRan.doDai.ToString();
            Diemp2.Text = conRan2.doDai.ToString();
        }
        private void GameOver()
        {
            PauseButton.Enabled = false;
            ResetButton.Enabled = true;
            cbbTime.Enabled = true;
            cbbMap.Enabled = true;
        }
        
        private void KetThuc()
        {
            MessageBox.Show("Finished!");
            PlayButton.Enabled = true;

        }
        void conRan_GoiSuLi2(Point[] p, Point tailLast)
        {
            if (p.Length < 100)
            {
                phamVi.Change(p, 4);
                phamVi.Change(tailLast, 0);
                int temp = phamVi.Check(p[0]);
                if (temp == 1)
                {
                    Go(false);                    
                    this.Invoke(gameOver);
                    MessageBox.Show("Player 1 Win");
                }
                else if (temp == 2)
                {
                    conRan2.add();
                    conRan2.add();
                    conRan2.add();
                    phamVi.Change(thucAn.ToaDo, 0);
                    do
                    {
                        thucAn.TaoDiem();
                    } while (!phamVi.avail(thucAn.ToaDo));
                    thucAn.redraw();
                    phamVi.Change(thucAn.ToaDo, 2);
                    
                }
                else if (temp == 5)
                {
                    conRan2.skill = itemmap.Instance;
                    //neu skill wall thi tao bien tuong
                    if (conRan2.skill == ItemMap.skills.wall)
                    {
                        w[0] = new wall();

                        Image image = (Image)Properties.Resources.wall;
                        pbskillp2.Image = image;
                    }
                    else if (conRan2.skill == ItemMap.skills.slow)
                    {
                        Image image = (Image)Properties.Resources.slow;
                        pbskillp2.Image = image;
                    }
                    else if (conRan2.skill == ItemMap.skills.bullet)
                    {
                        Image image = (Image)Properties.Resources.boom;
                        pbskillp2.Image = image;
                    }
                    //xoa item cu
                    phamVi.Change(itemmap.ToaDo, 0);
                    //tao item moi
                    do
                    {
                        itemmap.TaoDiem();
                    } while (!phamVi.avail(itemmap.ToaDo));
                    phamVi.Change(itemmap.ToaDo, 5);
                    itemmap.redraw();
                }
                thucAn.redraw();

            }
            else
            {
                Go(false);
                this.Invoke(ketThuc);
            }
        }

        void conRan_GoiSuLi(Point[] p, Point tailLast)
        {
            if (p.Length < 100)
            {
                phamVi.Change(p, 3);
                phamVi.Change(tailLast, 0);
                int temp = phamVi.Check(p[0]);

                if (temp == 1)
                {
                    Go(false);                   
                    this.Invoke(gameOver);
                    MessageBox.Show("Player 2 Win");
                }
                else if (temp == 2)
                {
                    conRan.add();
                    conRan.add();
                    conRan.add();
                    phamVi.Change(thucAn.ToaDo, 0);
                    do
                    {
                        thucAn.TaoDiem();
                    } while (!phamVi.avail(thucAn.ToaDo));
                    thucAn.redraw();
                    phamVi.Change(thucAn.ToaDo, 2);
                    
                }
                else if (temp == 5)
                {
                    conRan.skill = itemmap.Instance;
                    //neu skill wall thi tao bien tuong
                    if (conRan.skill == ItemMap.skills.wall)
                    {
                        w[0] = new wall();

                        Image image = (Image) Properties.Resources.wall;
                        pbskillp1.Image = image;
                    }
                    else if (conRan.skill == ItemMap.skills.slow)
                    {
                        Image image = (Image)Properties.Resources.slow;
                        pbskillp1.Image = image;
                    }
                    else if (conRan.skill == ItemMap.skills.bullet)
                    {
                        Image image = (Image)Properties.Resources.boom;
                        pbskillp1.Image = image;
                    }
                    //xoa item cu
                    phamVi.Change(itemmap.ToaDo, 0);
                    //tao item moi
                    do
                    {
                        itemmap.TaoDiem();
                    } while (!phamVi.avail(itemmap.ToaDo));
                    phamVi.Change(itemmap.ToaDo, 5);
                    itemmap.redraw();
                }
                thucAn.redraw();
            }
            else
            {
                Go(false);
                this.Invoke(ketThuc);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool bHandled = false;
            Image image;
            switch (keyData)
            {
                case Keys.Right:
                    conRan.diChuyen = DiChuyen.phai;
                    bHandled = true;
                    break;
                case Keys.Left:
                    conRan.diChuyen = DiChuyen.trai;
                    bHandled = true;
                    break;
                case Keys.Up:
                    conRan.diChuyen = DiChuyen.len;
                    bHandled = true;
                    break;
                case Keys.Down:
                    conRan.diChuyen = DiChuyen.xuong;
                    bHandled = true;
                    break;
                case Keys.D:
                    conRan2.diChuyen = DiChuyen.phai;
                    bHandled = true;
                    break;
                case Keys.A:
                    conRan2.diChuyen = DiChuyen.trai;
                    bHandled = true;
                    break;
                case Keys.W:
                    conRan2.diChuyen = DiChuyen.len;
                    bHandled = true;
                    break;
                case Keys.S:
                    conRan2.diChuyen = DiChuyen.xuong;
                    bHandled = true;
                    break;
                case Keys.P:
                    ItemMap.skills ki = conRan.skill;
                    switch (ki)
                    {
                        case ItemMap.skills.bullet:
                            b[0] = new Bullet(g, conRan.viTri[1], conRan.diChuyen);
                            image = (Image) Properties.Resources.none;
                            pbskillp1.Image = image;
                            conRan.skill=ItemMap.skills.none;
                            break;
                        case ItemMap.skills.wall:
                            if (w[0] != null)
                                w[0].draw(g, conRan.tailLast);
                            phamVi.Change(conRan.tailLast, 1);
                            if (w[0].count == 0)
                            {
                                conRan.skill = ItemMap.skills.none;
                                image = (Image)Properties.Resources.none;
                                pbskillp1.Image = image;
                                w[0] = null;
                            }
                            break;
                        case ItemMap.skills.slow:
                            slow[0] = 20;
                            conRan.skill = ItemMap.skills.none;
                            image = (Image)Properties.Resources.none;
                            pbskillp1.Image = image;
                            break;
                    }
                    break;
                case Keys.V:
                    ItemMap.skills k = conRan2.skill;
                    switch (k)
                    {
                        case ItemMap.skills.bullet:
                            b[1] = new Bullet(g, conRan2.viTri[1], conRan2.diChuyen);
                            conRan2.skill = ItemMap.skills.none;
                            image = (Image)Properties.Resources.none;
                            pbskillp2.Image = image;
                            break;
                        case ItemMap.skills.wall:
                            if (w[1] != null)
                                w[1].draw(g, conRan2.tailLast);
                            phamVi.Change(conRan2.tailLast, 1);
                            if (w[1].count == 0)
                            {
                                conRan2.skill = ItemMap.skills.none;
                                image = (Image) Properties.Resources.none;
                                pbskillp2.Image = image;
                                w[1] = null;
                            }
                            break;
                        case ItemMap.skills.slow:
                            slow[1] = 20;
                            conRan2.skill = ItemMap.skills.none;
                            image = (Image)Properties.Resources.none;
                            pbskillp2.Image = image;
                            break;
                    }
                    break;
            }
            return bHandled;
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            Go(true);
            PlayButton.Enabled = false;
            PauseButton.Enabled = true;
            ResetButton.Enabled = false;
            cbbMap.Enabled = false;
            timer = Int32.Parse(cbbTime.SelectedItem.ToString());
            txtTime.Text = timer.ToString();
            cbbTime.Enabled = false;

        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            if (conRan != null)
                if (PauseButton.Text == "Pause")
                {
                    PauseButton.Text = "Resume";
                    ResetButton.Enabled = true;
                    Go(false);
                }
                else
                {
                    PauseButton.Text = "Pause";
                    ResetButton.Enabled = false;
                    Go(true);
                }
        }
        private void ResetButton_Click(object sender, EventArgs e)
        {
            reset();
            PlayButton.Enabled = true;
            PauseButton.Enabled = false;
            PauseButton.Text = "Pause";
            cbbMap.Enabled = true;
            cbbTime.Enabled = true;
        }
        private void reset()
        {
            vc = new VatCan(g);
            vc.DocFile(mapType);
            phamVi = new MaTran();
            thucAn = new ThucAn(g);
            itemmap = new ItemMap(g);
            if (conRan != null) conRan.Dispose();
            conRan = new snake(g, new Point(10, 0), Brushes.DarkGreen, Brushes.LightPink, Pens.BlueViolet);
            if (conRan2 != null) conRan2.Dispose();
            conRan2 = new snake(g, new Point(10, 49), Brushes.Red, Brushes.LightBlue, Pens.BlueViolet);
            phamVi.Change(vc.vitri, 1);
            do
            {
                thucAn.TaoDiem();
            } while (!phamVi.avail(thucAn.ToaDo));
            phamVi.Change(thucAn.ToaDo, 2);

            do
            {
                itemmap.TaoDiem();
            } while (!phamVi.avail(itemmap.ToaDo));
            phamVi.Change(itemmap.ToaDo, 5);

            g.Clear(Color.White);
            vc.draw();
            Diemp1.Text = "0";

            conRan.ve();
            conRan2.ve();
            thucAn.redraw();
            itemmap.redraw();
            tocDo = new Timer(200);
            tocDo.Enabled = false;
            tocDo.Elapsed += new ElapsedEventHandler(tocDo_Elapsed);

            Image image = (Image)Properties.Resources.none;
            pbskillp2.Image = image;
            pbskillp1.Image = image;
        }

        private void tocDo_Elapsed(object sender, ElapsedEventArgs e)
        {
            MainGame();
            this.Invoke(demTime);
        }

        void snakeDisappear(snake ran)
        {
            for (int i = ran.doDai; i >= 1; i--)
            {
                render.Draw(g, new Point(ran.viTri[i].X * 10, ran.viTri[i].Y * 10), Pens.White, Brushes.White);

            }
            phamVi.Change(ran.viTri, 0);
        }
        void diappearAll()
        {
            snakeDisappear(conRan);
            snakeDisappear(conRan2);
        }

        public void Go(bool b)
        {
            tocDo.Enabled = b;
        }

        private void cbbMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] a = new string[5] { "level1.lev", "level2.lev", "level3.lev", "level4.lev", "level5.lev" };
            mapType = a[cbbMap.SelectedIndex];
            reset();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            vc.draw();
            conRan.ve();
            conRan2.ve();
            thucAn.redraw();
            itemmap.redraw();
        }
    }
}
