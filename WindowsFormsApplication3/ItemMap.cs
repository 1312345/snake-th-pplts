﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GamesSnake
{
    public class ItemMap
    {
        public enum skills
        {
            none,
            bullet,
            wall,
            slow
        }

        private Point toaDo;
        public Point ToaDo { get { return toaDo; } }
        private Graphics g;
        Random rd = new Random();
        public skills Instance = skills.none;

        public ItemMap(Graphics gr)
        {
            g = gr;
            toaDo.X = rd.Next(50);
            toaDo.Y = rd.Next(50);
            skills[] a = new skills[3] {skills.bullet, skills.slow, skills.wall};
            Instance = a[rd.Next(0,2)];
        }
        //public void draw()
        //{
        //    render.Draw(g, new Point(toaDo.X * 10, toaDo.Y * 10), Pens.White, Brushes.White);
        //    toaDo.X = rd.Next(50);
        //    toaDo.Y = rd.Next(50);
        //    render.Draw(g, new Point(toaDo.X*10,toaDo.Y*10), Pens.Orange, Brushes.YellowGreen);
        //}
        public void redraw()
        {
            render.Draw(g, new Point(toaDo.X * 10, toaDo.Y * 10), Pens.DarkOrange, Brushes.Indigo);
        }
        public Point TaoDiem()
        {
            toaDo.X = rd.Next(50);
            toaDo.Y = rd.Next(50);
            skills[] a = new skills[3] {skills.bullet, skills.slow, skills.wall };
            Instance = a[rd.Next(0, 2)];
            
            return toaDo;
        }
    }
}

