﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Timers;

namespace GamesSnake
{
    //Download source code tại Sharecode.vn
    public class snake : IDisposable
    {
        #region thành phần dữ liệu
        /// <summary>
        /// mảng chứa vị trí các nút(thân) của con rắn.
        /// với phần tử vitri[1] là đầu rắn
        /// phần tử cuối là đuôi
        /// </summary>
        public Point[] viTri = new Point[120];
        /// <summary>
        /// do dai cua con ran mat dinh bang 3
        /// </summary>
        public int doDai;
        /// <summary>
        /// màu sắc đầu con rắn
        /// </summary>
        Brush dauRan;
        /// <summary>
        /// màu sắc thân con rắn
        /// </summary>
        Brush thanRan;
        /// <summary>
        /// màu sắc đường viền ngoài con rắn
        /// </summary>
        Pen vienNgoai;
        /// <summary>
        /// hướng di chuyển của rắn, trái|phải|trên|dưới
        /// </summary>
        public DiChuyen diChuyen { get; set; }

        /// <summary>
        /// nơi cần vẽ lên|nơi con rắn ở :D
        /// </summary>
        Graphics g;

        public ItemMap.skills skill = ItemMap.skills.none;  
        public Point tailLast;

        #endregion

        /// <summary>
        /// khởi tạo các giá trị
        /// </summary>
        /// <param name="gr">vùng dùng để vẽ con ran len</param>
        /// <param name="vTri">tọa độ ban đầu cần vẽ(cái đầu của nó :D)</param>
        /// <param name="dr">màu sắc cái đầu rắn</param>
        /// <param name="th">màu sắc thân rắn</param>
        /// <param name="vn">màu sắc đường viền con rắn :D</param>
        public snake(Graphics gr, Point vTri, Brush dr, Brush th, Pen vn)
        {
            g = gr;
            doDai = 10;
            dauRan = dr;
            thanRan = th;
            vienNgoai = vn;
            for (int i = doDai; i > 0; i--)
                viTri[i] = new Point((vTri.X - i), vTri.Y);
            diChuyen = DiChuyen.phai;

        }
        #region các phương thức 

        public void ve()
        {
            for (int i = 2; i <= doDai; i++)
                //ve phan than con ran
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), vienNgoai, thanRan);
            //ve dau ran
            render.Draw(g, new Point(viTri[1].X * 10, viTri[1].Y * 10), vienNgoai, dauRan);
        }

        public void move()
        {
            tailLast = viTri[doDai]; //new Point(viTri[doDai].X * 10, viTri[doDai].Y * 10);
            //xoa duoi
            render.Draw(g, new Point(viTri[doDai].X * 10, viTri[doDai].Y * 10), Pens.White, Brushes.White);
            //ve tu duoi ve toi dau
            for (int i = doDai; i > 1; i--)
            {
                viTri[i] = viTri[i - 1];
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), vienNgoai, thanRan);
            }
            //ve dau ran
            if (diChuyen == DiChuyen.trai)
                viTri[1].X -= 1;
            else if (diChuyen == DiChuyen.phai)
                viTri[1].X += 1;
            if (diChuyen == DiChuyen.len)
                viTri[1].Y -= 1;
            else if (diChuyen == DiChuyen.xuong)
                viTri[1].Y += 1;
            render.Draw(g, new Point(viTri[1].X * 10, viTri[1].Y * 10), vienNgoai, dauRan);

        }
        public void add()
        {
            doDai += 1;
            viTri[doDai] = new Point(viTri[doDai - 1].X, viTri[doDai - 1].Y);
        }

        public void Eat(int l)
        {
            for (int i = 1; i <= l; i++)
            {
                add();
            }
        }

        public void beEatten(int l)
        {
            doDai -= l;
            var foos = new List<Point>(viTri);
            for (int i = 1; i <= l; i++)
            {
                foos.RemoveAt(doDai);
            }
            viTri = foos.ToArray();
        }
        public int SnakeEatSnake(snake SnakeEatten)
        {
            Point head = viTri[1];
            Point[] Snake2 = SnakeEatten.viTri;
            for (int i = 2; i < Snake2.Length; i++)
                if (Snake2[i] == head)
                    return i;
            return -1;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            for (int i = 2; i <= doDai; i++)
                render.Draw(g, new Point(viTri[i].X * 10, viTri[i].Y * 10), Pens.White, Brushes.White);
            render.Draw(g, new Point(viTri[1].X * 10, viTri[1].Y * 10), Pens.White, Brushes.White);
        }

        #endregion
    }
}
