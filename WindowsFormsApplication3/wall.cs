﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GamesSnake
{
    public class wall
    {
        public int count;

        public wall()
        {
            count = 10;
        }

        public void draw(Graphics g, Point p)
        {
            if(count == 0)
                return;
            render.Draw(g, new Point(p.X * 10, p.Y * 10), Pens.LightCyan, Brushes.MediumSeaGreen);
            count--;
        }
    }
}
